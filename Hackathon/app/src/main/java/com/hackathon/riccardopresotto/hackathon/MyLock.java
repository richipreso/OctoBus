package com.hackathon.riccardopresotto.hackathon;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by mattiaducci on 21/10/17.
 */

class MyLock {
    private static MyLock ourInstance = new MyLock();

    public static MyLock getInstance() {
        if (ourInstance == null) {
            ourInstance = new MyLock();
        }
        return ourInstance;
    }

    private ReentrantLock reentrantLock = new ReentrantLock();

    private MyLock() {

    }

    public boolean lock() {
        return reentrantLock.tryLock();
    }

    public void unlock() {
        if (reentrantLock.isHeldByCurrentThread()) reentrantLock.unlock();
    }
}
