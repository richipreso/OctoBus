package com.hackathon.riccardopresotto.hackathon;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.widget.Toast;

import static android.content.Context.SENSOR_SERVICE;

/**
 * Created by riccardopresotto on 14/10/17.
 */

public class SensorManagerService implements SensorEventListener {
    private Context ctx;
    private Sensor accelerometer;
    private SensorManager sensorManager;
    private float lastUpdate;
    private float lastShaking;
    private float lastAngolazione;
    private float acc;
    private float accCurr;
    private float accLast;

    private final long UPDATE_EVENT_THRESHOLD = 100000000L;   // nanosecondi
    private final long ACCELERATION_THRESHOLD = 6;           // metri al secondo quadro
    private final long FIRE_EVENT_THRESHOLD = 2500000000L;    // nanosecondi

    public SensorManagerService(Context context) {
        ctx = context;
        sensorManager = (SensorManager) ctx.getSystemService(SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        lastUpdate = lastShaking = lastAngolazione = 0;
        acc = 0.0f;
        accCurr = SensorManager.GRAVITY_EARTH;
        accLast = SensorManager.GRAVITY_EARTH;
    }

    public void startSensor() {
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void stopSensor() {
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        long timestamp = event.timestamp;

        // controllo dello shaking e dell'angolazione
        if (timestamp - lastUpdate > UPDATE_EVENT_THRESHOLD) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            accLast = accCurr;
            accCurr = (float)Math.sqrt((double)(x*x + y*y + z*z));
            float delta = accCurr - accLast;
            acc = acc * 0.9f + delta;   // perform low-cut filter

            // controllo accelerazione
            if (timestamp - lastShaking > FIRE_EVENT_THRESHOLD) {
                if (acc > ACCELERATION_THRESHOLD) {
                    //Toast.makeText(ctx, "shake detected w/ acc: " + acc, Toast.LENGTH_SHORT).show();
                    ((MainActivity) ctx).toggleSearching();
                    lastShaking = timestamp;
                }
            }

            // controllo angolazione
            if (timestamp - lastAngolazione > FIRE_EVENT_THRESHOLD) {
                ((MainActivity) ctx).setCorrectView(isInsideView(x, y, z));
                lastAngolazione = timestamp;
            }


            lastUpdate = timestamp;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    // se il posizinamento e' corretto ritorna 0
    // se devi alzare il telefono torna 1
    // se devi abbassare il telefono torna -1
    public int isInsideView(float x, float y, float z) {
        // il telefono deve guardare l'orizzonte con una inclinazione massima di 45 gradi
        // questo e' controllato dalla componente z che deve essere tra -5 e +5
        if (z <= -5) return -1;

        if (z >= 5) return 1;

        return 0;
    }
}
