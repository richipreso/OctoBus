package com.hackathon.riccardopresotto.hackathon;

import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;

import com.ibm.watson.developer_cloud.visual_recognition.v3.VisualRecognition;
import com.ibm.watson.developer_cloud.visual_recognition.v3.model.ClassifiedImages;
import com.ibm.watson.developer_cloud.visual_recognition.v3.model.ClassifyOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.concurrent.locks.Lock;


/**
 * Created by mattiaducci on 14/10/17.
 */

public class RestAsyncTask extends AsyncTask<URL,Void,String> {
    File file;

    public RestAsyncTask(File file_) {
        file = file_;
    }

    @Override
    protected String doInBackground(URL... params) {
        if (MyLock.getInstance().lock()) {
            VisualRecognition service = new VisualRecognition(VisualRecognition.VERSION_DATE_2016_05_20);
            service.setApiKey("f5bab68bb0303d559805af8eb8e5f7dddecdc568");
            InputStream imagesStream = null;
            try {
                imagesStream = new FileInputStream(file);
            } catch (FileNotFoundException e) {
            }

            ClassifyOptions classifyOptions = new ClassifyOptions.Builder()
                    .imagesFile(imagesStream)
                    .imagesFilename("imgciao.jpg")
                    .parameters("{\"classifier_ids\": [\"bus_423169041\"],"
                            + "\"owners\": [\"ea94ae50-2c10-435d-8b28-c699ea3395c2\"],"
                            + "\"threshold\":" + 0.2 + "}")
                    .build();
            ClassifiedImages result = service.classify(classifyOptions).execute();
            //Log.e("RISULTATO NUOVA", result.toString());

            MyLock.getInstance().unlock();
            return String.valueOf(result);
        } else {
            return null;
        }
    }

    @Override
    protected void onPostExecute(String s) {
        if (s != null) {
            Log.e("risposta rest", s.toString());
            String[] split = s.split("_");
            String res = split[0];
            //String contenuto = split[1];
            //Log.e("WATSON",contenuto);
            //switcho sulla risposta della richiesta
            switch (res) {
                case "0": // errore
                    Log.e("Errore", "0");
                    break;
                case "200": // ok
                    Log.e("ADV", s);
                    break;
                case "400":
                    Log.e("Errore", "400");
                    break;
                case "406": // not acceptable -> ourPwd sbagliata, quindi per malintenzionati
                    Log.e("Errore", ": not acceptable 406");
                    break;
                case "500": // not acceptable -> internal server error
                    Log.e("Errore", ": internal server error 500");
                    break;
                default:

                    if (s.contains("93")) Log.e("LINEA", "trovata");
                    else Log.e("LINEA", "non trovata");
                    break;

            }
        }
    }


}
