package com.hackathon.riccardopresotto.hackathon;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Handler;
import android.os.Vibrator;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static android.content.ContentValues.TAG;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;
import static com.hackathon.riccardopresotto.hackathon.CameraPreview.mPicture;

public class MainActivity extends Activity {

    TextView txtNumber;
    TextView txtText;
    private static Camera mCamera;
    private CameraPreview mPreview;
    // attrezzatura per il sensore, lo shaking e l'interfaccia
    SensorManagerService sms;
    boolean searchingActive;
    Vibrator vib;
    boolean primoAvvio=true;

    //TextToSpeech tts;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtText = (TextView)findViewById(R.id.txt_text);
        txtNumber = (TextView)findViewById(R.id.txt_number);
        txtText.announceForAccessibility("Alza il telefono");
        // Create an instance of Camera
        mCamera = getCameraInstance();
        // Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(this, mCamera);
        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview);
        sms = new SensorManagerService(this);
        sms.startSensor();
        searchingActive = false;
        vib = (Vibrator)getSystemService(VIBRATOR_SERVICE);

    }

    @Override
    protected void onPause() {
        super.onPause();
        sms.stopSensor();
        searchingActive = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        sms.startSensor();
    }

    /** Check if this device has a camera */
    private boolean checkCameraHardware(Context context) {
        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            //Log.e("camera_","OK");
            return true;
        } else {
            Log.e("camera_","FAIL");
            return false;
        }
    }

    /** A basic Camera preview class */


    /** A safe way to get an instance of the Camera object. */
    public Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
            Log.e("camera","dont exist");
        }
        return c;
    }
    // returns null if camera is unavailable


    public void setCorrectView(int isCorrect) {
        if (searchingActive) {
            long[] pattern = {0, 100, 100, 400};
            switch (isCorrect) {
                case 0:
                    txtText.setText("Scuoti per interrompere la ricerca");
                    break;
                case 1:
                    txtText.setText("Alza il telefono");
                    txtText.announceForAccessibility("Alza il telefono");
                    vib.vibrate(pattern, -1);
                    break;
                case -1:
                    txtText.setText("Abbassa il telefono");
                    txtText.announceForAccessibility("Abbassa il telefono");
                    vib.vibrate(pattern, -1);
                    break;
            }
        } else {
            txtText.setText("Scuoti per avviare la ricerca di un autobus");

            if(primoAvvio) {
                txtText.announceForAccessibility("Scuoti per avviare la ricerca di un autobus");
                primoAvvio=false;
            }
        }
    }


    public void stopPhoto(){

    }

    public void toggleSearching() {
        searchingActive = !searchingActive;

        if(searchingActive) {

            Thread t = new Thread(new Runnable() {
                    public void run() {

                        try {
                            Thread.sleep(3000);
                            while (searchingActive) {
                                Thread.sleep(1000);

                                mCamera.takePicture(null, null, mPicture);

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                if(searchingActive) {
                    t.start();
                }
            //Toast.makeText(this, "Shake ON", Toast.LENGTH_SHORT).show();
        }else {
            //Toast.makeText(this, "Shake OFF", Toast.LENGTH_SHORT).show();
        }

        if(!searchingActive)primoAvvio=true;

    }

}


